#ifndef __CAR_H__
#define __CAR_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "typedefs.h"
#include "carId.h"

/**
 * @brief - function to parse the message to get the Car Structure. The message should be the game init message
 * @return - pointer to Car Structure
 */
Car** GetCars(cJSON* msg, int* carCount);

#endif /* __CAR_H__ */
