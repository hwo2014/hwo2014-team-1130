#ifndef __CRASH_H__
#define __CRASH_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "typedefs.h"
#include "dbg.h"
#include "cJSON.h"

int HasCarCrashed(cJSON* msg, CarId* id);

#endif /* __CRASH_H__ */

