#ifndef __CAR_ID_H__
#define __CAR_ID_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"
#include "typedefs.h"
#include "carPosition.h"


/**
 * @brief - function to parse a JSON object to get CarId
 * @return - retuns a pointer to CarId structure
 */
CarId* GetCarId(cJSON* msg);

/**
 * @brief - function to free the memory allocated for the CarId structure
 * @return - void
 */
void DisposeCarId(CarId* id);

/**
 * @brief - function to compare two car id structures
 * @return - returns zero if the id's match
 */
int CompareCarId(CarId* id1, CarId* id2);

#endif /* __CAR_ID_H__ */
