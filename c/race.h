#ifndef __RACE_H__
#define __RACE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "typedefs.h"
#include "cJSON.h"


/**
 * @brief - input is the gameInit msg
 */
RaceSession* GetRaceSession(cJSON* msg);

void DisposeRaceSession(RaceSession*);

void PrintRaceSession(RaceSession*);

#endif /* __RACE__H__ */
