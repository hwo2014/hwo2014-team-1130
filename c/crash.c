#include "crash.h"

/**
    {"msgType": "crash", "data": {
     "name": "Rosberg",
     "color": "blue"
    }, "gameId": "OIUHGERJWEOI", "gameTick": 3}
 */
int HasCarCrashed(cJSON* msg, CarId* id)
{
    char* name = msg->next->child->valuestring;
    char* color = msg->next->child->next->valuestring;

    if((strcmp(name, id->name)==0) && (strcmp(color, id->color)))
        return 1;
    else
        return 0;
}
