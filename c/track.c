#include "track.h"
#include "cJSON.h"

/**
 * @func GetTrack
 * @brief To generate the track from JSON data
 * The track data is given as a JSON object.
 * An example is shown below
 * msgType": "gameInit", "data": {
 * "race": {
 * "track": {
 *      "id": "indianapolis",
 *      "name": "Indianapolis",
 *      "pieces": [
 *          {
 *              "length": 100.0
 *          },
 *          {
 *              "length": 100.0,
 *              "switch": true
 *          },
 *          {
 *              "radius": 200,
 *              "angle": 22.5
 *          }]
 * @args pointer to JSON data
 * @return pointer to Track structure
 */
Track* GetTrack(cJSON* data)
{
    Track* track = (Track*)malloc(sizeof(Track));
    cJSON* dataJSON = NULL;
    cJSON* raceJSON = NULL;
    cJSON* trackJSON = NULL;
    cJSON* idJSON = NULL;
    cJSON* nameJSON = NULL;
    cJSON* piecesJSON = NULL;
    cJSON* pieceJSON = NULL;
    cJSON* pieceContentJSON = NULL;
    cJSON* laneItemJSON = NULL;
    cJSON* lanesJSON = NULL;
    int index = 0;

    memset(track, 0, sizeof(Track));

    if(data != NULL)
    {
        dataJSON = data->next;
        raceJSON = cJSON_GetObjectItem(dataJSON, "race");
        if(raceJSON != NULL)
        {
            trackJSON = cJSON_GetObjectItem(raceJSON, "track");
            if(trackJSON != NULL)
            {
                idJSON = cJSON_GetObjectItem(trackJSON, "id");
                nameJSON = cJSON_GetObjectItem(trackJSON, "name");
                piecesJSON = cJSON_GetObjectItem(trackJSON, "pieces");
                lanesJSON = cJSON_GetObjectItem(trackJSON, "lanes");

                if(idJSON != NULL)
                {
                    track->id = (char*) malloc(strlen(idJSON->valuestring));
                    strcpy(track->id, idJSON->valuestring);
                }

                if(nameJSON != NULL)
                {
                    track->name = (char*) malloc(strlen(nameJSON->valuestring));
                    strcpy(track->name, nameJSON->valuestring);
                }

                if(piecesJSON != NULL)
                {
                    track->count = cJSON_GetArraySize(piecesJSON);
                    track->pieces = (TrackPiece*)malloc(track->count * sizeof(TrackPiece));
                    memset(track->pieces, 0, sizeof(TrackPiece) * track->count);

                    for(index=0; index<track->count; index++)
                    {
                        pieceJSON = cJSON_GetArrayItem(piecesJSON, index);

                        pieceContentJSON = pieceJSON->child;
                        if(strcmp(pieceContentJSON->string, "length") == 0)
                        {
                            track->pieces[index].type = Straight;
                            track->pieces[index].length = pieceContentJSON->valueint;
                            if(pieceContentJSON->next != NULL)
                            {
                                track->pieces[index].hasSwitch = (pieceContentJSON->next->type == cJSON_True) ? 1 : 0;
                            }
                        }
                        else
                        {
                            track->pieces[index].type = Bend;
                            track->pieces[index].radius = pieceContentJSON->valueint;
                            track->pieces[index].angle = pieceContentJSON->next->valuedouble;
                        }
                    }
                }

                if(lanesJSON != NULL)
                {
                    track->laneCount = cJSON_GetArraySize(lanesJSON);
                    track->lanes = (Lane*)malloc(track->laneCount * sizeof(Lane));

                    for(index=0; index<track->laneCount; index++)
                    {
                        laneItemJSON = cJSON_GetArrayItem(lanesJSON, index);
                        track->lanes[index].distanceFromCenter = laneItemJSON->child->valueint;
                        track->lanes[index].index = laneItemJSON->child->next->valueint;
                    }
                }
            }
        }
    }
    return track;
}


/**
 * @func - DisposeTrack
 * @brief - To free the memory allocated for the track
 * @args - pointer to track structure.
 * @return - void
 */
void DisposeTrack(Track* track)
{
    if(track != NULL)
    {
        if(track->pieces != NULL)
            free(track->pieces);
        if(track->id != NULL)
            free(track->id);
        if(track->name != NULL)
            free(track->name);
        if(track != NULL)
            free(track);
    }
}
