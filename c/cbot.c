#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include "cJSON.h"
#include "track.h"
#include "car.h"
#include "carId.h"
#include "carPosition.h"
#include "race.h"
#include "lap.h"
#include "gameAI.h"
#include "dbg.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *joinRace_msg(char *bot_name, char *bot_key, char *trackName);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON* create_useturbo_msg();
static cJSON* create_laneswitch_msg(short changeLane);
static cJSON* throttle_sync_msg(double throttle, int gametick);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if(!strcmp("gameInit", msg_type_name)) {
        puts("Received game init");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("lapFinished", msg_type_name)) {
        puts("Lap finished");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    } else {
	    printf("->> Received %s \n",msg_type_name);
    }

}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;
    Track *track;
    Car **cars = NULL;
    Car *myCar = NULL;
    CarId *carId;
    CarPosition* currentPos = NULL;
    CarPosition** carPositions;

    RaceSession* raceSession;
    int carCount = 0;
    double throttle = 0.6;
    short changeLane = 0;
    int carPosIndex = 0;
    int carIndex = 0;
    int lapIndex = 1;
    int turboAvailable = 0;
    int turboUsableTrackIndex = 0;
    int prevIndex = 0;
    double velocity;
    double cumulativeDist = 0;
    double distCovered = 0;

    if (argc < 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    if(argc == 5)
        json = join_msg(argv[3], argv[4]);
    else
        json = joinRace_msg(argv[3], argv[4], argv[5]);

    write_msg(sock, json);
    cJSON_Delete(json);

    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;

        if (!strcmp("carPositions", msg_type_name))
        {
            GetCarPositions(msg_type, carPositions, carCount);
            carPosIndex = GetCarPosition(carPositions, carCount, carId);
            currentPos = carPositions[carPosIndex];

            if(prevIndex != currentPos->piecePos->index)
            {
                if(track->pieces[prevIndex].type == Straight)
                    cumulativeDist += track->pieces[prevIndex].length;
                else
                    cumulativeDist += (0.0174532925 * track->pieces[prevIndex].angle * track->pieces[prevIndex].radius);
            }

            velocity = (cumulativeDist + currentPos->piecePos->inPieceDistance - distCovered);
            distCovered = cumulativeDist + currentPos->piecePos->inPieceDistance;

            prevIndex = currentPos->piecePos->index;
            GetOptimalSettings(track, carPositions, carPosIndex, carCount, myCar, velocity, velocity, &throttle, &changeLane);
            if(changeLane != 0)
            {
                msg = create_laneswitch_msg(changeLane);
//                if(turboAvailable == 1 && lapIndex == raceSession->laps && currentPos->piecePos->index == turboUsableTrackIndex)
//                {
//                    msg = create_useturbo_msg();
//                    turboAvailable = 0;
//                }
//                else
//                {
//                    msg = ping_msg();
//                }
            }
            else
            {
                if(turboAvailable == 1 && lapIndex == raceSession->laps && currentPos->piecePos->index == turboUsableTrackIndex)
                {
                    msg = create_useturbo_msg();
                    turboAvailable = 0;
                }
                else
                {
                    msg = throttle_sync_msg(throttle, currentPos->tick);
                }
            }
        }
        else if (!strcmp("gameInit", msg_type_name))
        {
            puts("Received game init");
            track = GetTrack(msg_type);

            cars = GetCars(msg_type, &carCount);
            for(carIndex = 0; carIndex < carCount; carIndex++)
            {
                if(CompareCarId(carId, cars[carIndex]->id) == 0)
                {
                    myCar = cars[carIndex];
                    break;
                }
            }
            printf("------------------------\n");
            printf("My car details:\n");
            printf("Id: %s %s\n", myCar->id->name, myCar->id->color);
            printf("Length: %lf\n", myCar->dimension->length);
            printf("Width: %lf\n", myCar->dimension->width);
            printf("Guide flag position: %lf\n", myCar->dimension->guideFlagPosition);
            printf("------------------------\n");


            raceSession = GetRaceSession(msg_type);
            PrintRaceSession(raceSession);

            carPositions = (CarPosition**)malloc(sizeof(CarPosition*) * carCount);
            for(carPosIndex=0; carPosIndex<carCount; carPosIndex++)
                carPositions[carPosIndex] = (CarPosition*)malloc(sizeof(CarPosition));

            turboUsableTrackIndex = GetTurboUsableTrackIndex(track);

            msg = ping_msg();
        }
        else if(!strcmp("yourCar", msg_type_name))
        {
            puts("Received yourCar");
            carId = GetCarId(msg_type->next);
            msg = ping_msg();
        }
        else if(!strcmp("lapFinished", msg_type_name))
        {
            lapIndex++;
            PrintLapStatistics(msg_type);
            msg = ping_msg();
        }
//        else if(!strcmp("crash", msg_type_name))
//        {
//            if(HasCarCrashed(msg_type, carId))
//            {
//            }
//            msg = ping_msg();
//        }
        else if(!strcmp("turboAvailable", msg_type_name))
        {
            turboAvailable = 1;
            msg = ping_msg();
        }
        else if(!strcmp("gameStart", msg_type_name))
        {
            msg = throttle_msg(1.0);
        }
        else
        {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

/**
If you just want to drive in Germany alone (who wouldn’t?) you can
{"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "germany",
  "carCount": 1
}}
*/
static cJSON *joinRace_msg(char *bot_name, char *bot_key, char *trackName)
{
    cJSON* botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);

    cJSON *data = cJSON_CreateObject();
    cJSON_AddItemToObject(data, "botId", botId);
    cJSON_AddStringToObject(data, "trackName", trackName);
    cJSON_AddNumberToObject(data, "carCount", 1);

    return make_msg("joinRace", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON* throttle_sync_msg(double throttle, int gametick)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", "throttle");
    cJSON_AddItemToObject(json, "data", cJSON_CreateNumber(throttle));
    cJSON_AddItemToObject(json, "gameTick", cJSON_CreateNumber(gametick));
    return json;
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}


/**
{"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
*/
static cJSON* create_useturbo_msg()
{
    return make_msg("turbo", cJSON_CreateString("Wroooooom!"));
}

static cJSON* create_laneswitch_msg(short changeLane)
{
    if(changeLane == -1)
        return make_msg("switchLane", cJSON_CreateString("Right"));
    else
        return make_msg("switchLane", cJSON_CreateString("Left"));
}

