#ifndef __TRACK_H__
#define __TRACK_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "typedefs.h"
#include "cJSON.h"

Track* GetTrack(cJSON* data);

void DisposeTrack(Track* track);

#endif /* __TRACK_H__ */
