#ifndef __GAME_AI_H__
#define __GAME_AI_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "typedefs.h"
#include "carPosition.h"

/**
 * @brief - throttle and changeLane are output parameters.
 */
void GetOptimalSettings(Track* track, CarPosition** positions, int carPosIndex, int carCount, Car* car, double velocity, double dist, double* throttle, short* changeLane);

int GetTurboUsableTrackIndex(Track* track);

#endif /* __GAME_AI_H__ */


