#ifndef __LAP_H__
#define __LAP_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cJSON.h"
#include "typedefs.h"

void PrintLapStatistics(cJSON* msg);

#endif /* __LAP_H__ */

