#include "carId.h"

CarId* GetCarId(cJSON* msg)
{
    cJSON* nameJSON = NULL;
    cJSON* colorJSON = NULL;
    int nameLen = 0;
    int colorLen = 0;
    CarId* id = (CarId*)malloc(sizeof(CarId));
    memset(id, 0, sizeof(CarId));

    nameJSON = cJSON_GetObjectItem(msg, "name");
    colorJSON = cJSON_GetObjectItem(msg, "color");

    nameLen = strlen(nameJSON->valuestring);
    colorLen = strlen(colorJSON->valuestring);

    id->name = (char*)malloc(sizeof(char) * nameLen);
    id->color = (char*)malloc(sizeof(char) * colorLen);

    strcpy(id->name, nameJSON->valuestring);
    strcpy(id->color, colorJSON->valuestring);

    return id;
}

void DisposeCarId(CarId* id)
{
    if(id != NULL)
    {
        if(id->name != NULL)
            free(id->name);
        if(id->color != NULL)
            free(id->color);

        free(id);
    }
}

int CompareCarId(CarId* id1, CarId* id2)
{
    int retVal = 0;

    if(strcmp(id1->name, id2->name) != 0)
        retVal = 1;

    if(strcmp(id1->color, id2->color) != 0)
        retVal = 1;

    return retVal;
}

