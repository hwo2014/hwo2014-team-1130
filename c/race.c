#include "race.h"

RaceSession* GetRaceSession(cJSON* msg)
{
    cJSON* raceSessionJSON = NULL;
    RaceSession* raceSession = (RaceSession*) malloc(sizeof(RaceSession));
    memset(raceSession, 0, sizeof(RaceSession));

    /**
     * msgType -> data |
                       |
                        race |
                             |
                             track -> cars -> raceSession
     */
    raceSessionJSON = msg->next->child->child->next->next;

    raceSession->laps = cJSON_GetObjectItem(raceSessionJSON, "laps")->valueint;
    raceSession->maxLapTime = cJSON_GetObjectItem(raceSessionJSON, "maxLapTimeMs")->valueint;
    raceSession->quickRace = (cJSON_GetObjectItem(raceSessionJSON, "quickRace")->type == cJSON_True) ? 1 : 0;

    return raceSession;
}

void DisposeRaceSession(RaceSession* race)
{
    free(race);
    race = NULL;
}

void PrintRaceSession(RaceSession* race)
{
    printf("Race session: laps(%d) maxLapTime(%d) quickRace(%d)\n", race->laps, race->maxLapTime, race->quickRace);
}
