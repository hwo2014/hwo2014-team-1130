#include "car.h"

/*
"cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ]
    */


Car** GetCars(cJSON* msg, int* carCount)
{
    Car** cars = NULL;
    cJSON* dataJSON = NULL;
    cJSON* raceJSON = NULL;
    cJSON* carsJSON = NULL;
    cJSON* carJSON = NULL;
    cJSON* dimensionJSON = NULL;
    int carIndex = 0;

    printf("%s\n", cJSON_Print(msg));

    assert(carCount != NULL);
    assert(msg != NULL);
    assert(strcmp(cJSON_Print(msg), "\"gameInit\"") == 0);

    dataJSON = msg->next;

    raceJSON = cJSON_GetObjectItem(dataJSON, "race");

    carsJSON = cJSON_GetObjectItem(raceJSON, "cars");
    *carCount = cJSON_GetArraySize(carsJSON);
    printf("Number of cars: %d\n", *carCount);

    cars = (Car**)malloc(sizeof(Car*) * (*carCount));
    for(carIndex=0; carIndex<(*carCount); carIndex++)
    {
        cars[carIndex] = (Car*)malloc(sizeof(Car));
        carJSON = cJSON_GetArrayItem(carsJSON, carIndex);
        cars[carIndex]->id = GetCarId(cJSON_GetObjectItem(carJSON, "id"));

        cars[carIndex]->dimension = (CarDimension*)malloc(sizeof(CarDimension));
        dimensionJSON = cJSON_GetObjectItem(carJSON, "dimensions");
        cars[carIndex]->dimension->length = cJSON_GetObjectItem(dimensionJSON, "length")->valuedouble;
        cars[carIndex]->dimension->width = cJSON_GetObjectItem(dimensionJSON, "width")->valuedouble;
        cars[carIndex]->dimension->guideFlagPosition = cJSON_GetObjectItem(dimensionJSON, "guideFlagPosition")->valuedouble;
    }

    return cars;
}

/* @todo */
void DisposeCars()
{

}

