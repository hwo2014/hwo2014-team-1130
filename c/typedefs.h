#ifndef __TYPEDEFS_H__
#define __TYPEDEFS_H__

typedef struct
{
    int index;
    double inPieceDistance;
    int startLaneIndex;
    int endLaneIndex;
}PiecePosition;

typedef struct
{
    char* name;
    char* color;
}CarId;

typedef struct
{
    double length;
    double width;
    double guideFlagPosition;
}CarDimension;

typedef struct
{
    CarId* id;
    CarDimension* dimension;
}Car;

typedef struct
{
    CarId* id;
    double angle;
    PiecePosition *piecePos;
    long tick;
}CarPosition;

typedef struct
{
    int distanceFromCenter;
    int index;
}Lane;

typedef enum{
    Straight,
    Bend
}TrackPieceType;

typedef struct _trackPiece{
    TrackPieceType type;
    int length;
    short hasSwitch;
    double angle;
    int radius;
}TrackPiece;

typedef struct{
    TrackPiece* pieces;
    Lane* lanes;
    int laneCount;
    int count;
    char* id;
    char* name;
}Track;

typedef struct{
    int laps;
    int maxLapTime;
    short quickRace;
}RaceSession;

#endif /* __TYPEDEFS_H__ */
