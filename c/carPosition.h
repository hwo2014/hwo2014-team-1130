#ifndef __CAR_POSITION_H__
#define __CAR_POSITION_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "cJSON.h"
#include "typedefs.h"
#include "carId.h"

PiecePosition* GetPiecePos(cJSON* msg);

void GetCarPositions(cJSON*msg, CarPosition** positions, int carCount);

int GetCarPosition(CarPosition** carPositions, int carCount, CarId* id);

void PrintCarPosition(CarPosition* pos);

#endif /* __CAR_POSITION_H__ */
