#include "lap.h"

/**
{"msgType": "lapFinished", "data": {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}, "gameId": "OIUHGERJWEOI", "gameTick": 300}
*/

void PrintLapStatistics(cJSON* msg)
{
    cJSON* node = NULL;

    printf("------ Lap statistics ------\n");
    node = msg->next->child->child; // car
    printf("Car name: %s\n", node->valuestring);
    printf("Car color: %s\n", node->next->valuestring);
    node = msg->next->child->next; //laptime;
    printf("Lap #: %d\n", node->child->valueint);
    printf("Ticks : %d\n", node->child->next->valueint);
    printf("Millis : %d\n", node->child->next->next->valueint);
}
