#include "carPosition.h"
#include "carId.h"

/*
{"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}
*/


PiecePosition* GetPiecePos(cJSON* msg)
{
    PiecePosition* pos = (PiecePosition*)malloc(sizeof(PiecePosition));
    cJSON* pieceIndexJSON = NULL;
    cJSON* inPieceDistJSON = NULL;
    cJSON* laneJSON = NULL;
    cJSON* startLaneIndexJSON = NULL;
    cJSON* endLaneIndexJSON = NULL;

    pieceIndexJSON = cJSON_GetObjectItem(msg, "pieceIndex");
    pos->index = pieceIndexJSON->valueint;
    inPieceDistJSON = pieceIndexJSON->next;
    pos->inPieceDistance = inPieceDistJSON->valuedouble;

    laneJSON = inPieceDistJSON->next;
    startLaneIndexJSON = cJSON_GetObjectItem(laneJSON, "startLaneIndex");
    endLaneIndexJSON = cJSON_GetObjectItem(laneJSON, "endLaneIndex");
    pos->startLaneIndex = startLaneIndexJSON->valueint;
    pos->endLaneIndex = endLaneIndexJSON->valueint;

    return pos;
}

void PrintCarPosition(CarPosition* pos)
{
    printf("Id: %s %s Angle: %lf PiecePos: index(%d) inPieceDist(%lf)\n", pos->id->name, pos->id->color, pos->angle, pos->piecePos->index, pos->piecePos->inPieceDistance);
}

void GetCarPositions(cJSON*msg, CarPosition** positions, int carCount)
{
    cJSON* data = NULL;
    cJSON* cars = NULL;
    cJSON* car = NULL;
    cJSON* angleJSON = NULL;
    cJSON* piecePosJSON = NULL;
    int carIndex = 0;
    int gameTick = 0;

    assert(carCount > 0);

    data = msg->next;
    cars = data->child;

    if(msg->next->next->next != NULL)
        gameTick = msg->next->next->next->valueint;

    for(carIndex = 0; carIndex < carCount; carIndex++)
    {
        car = cJSON_GetArrayItem(cars, carIndex);
        angleJSON = car->next;
        piecePosJSON = angleJSON->next;

        positions[carIndex]->id = GetCarId(car);
        positions[carIndex]->angle = angleJSON->valuedouble;
        positions[carIndex]->piecePos = GetPiecePos(piecePosJSON);
        positions[carIndex]->tick = gameTick;
    }
}

int GetCarPosition(CarPosition** carPositions, int carCount, CarId* id)
{
    int carIndex = 0;
    for(carIndex = 0; carIndex < carCount; carIndex++)
    {
        if(CompareCarId(carPositions[carIndex]->id, id) == 0)
            break;
    }

    return carIndex;
}










