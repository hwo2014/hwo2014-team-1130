#include "gameAI.h"

void GetOptimalSettings(Track* track, CarPosition** positions, int carPosIndex, int carCount, Car* car, double velocity, double dist, double* throttle, short* changeLane)
{
    TrackPiece currentTrackPiece;
    TrackPiece nextTrackPiece;
    TrackPiece prevTrackPiece;

    CarPosition* currentPos;
    int laneIndex = 0;
    int currentTrackPieceIndex = 0;
    double beta=0, gamma=0, eta=0, psi=0, rho=0, kappa=0;
    double squaredLatVelocity = 0;
    static double lastVelocity = 0;
    double acceleration = 0.0;

    assert(throttle != NULL);
    assert(changeLane != NULL);

    currentPos = positions[carPosIndex];
    currentTrackPieceIndex = currentPos->piecePos->index;
    currentTrackPiece = track->pieces[currentTrackPieceIndex];
    nextTrackPiece = track->pieces[currentTrackPieceIndex+1];
    if(currentTrackPieceIndex > 0)
        prevTrackPiece = track->pieces[currentTrackPieceIndex-1];


    /** check if lane switching is possible or not */
    if(currentTrackPiece.hasSwitch)
    {
        for(laneIndex = 0; laneIndex < track->laneCount; laneIndex++)
        {
            if(track->lanes[currentPos->piecePos->startLaneIndex].distanceFromCenter != 0)
                *changeLane = -1;
        }
    }

    if(lastVelocity != 0)
        acceleration = velocity - lastVelocity;
    lastVelocity = velocity;

    if(currentTrackPiece.type == Straight)
    {
        if(currentPos->piecePos->inPieceDistance < 0.01 * currentTrackPiece.length)
        {
            if(currentTrackPieceIndex > 0)
                *throttle = (prevTrackPiece.type == Straight) ? 1.0 : 0.8;
            else
                *throttle = 1.0;
        }
        else if(currentPos->piecePos->inPieceDistance > 0.95 * currentTrackPiece.length)
        {
            *throttle = (nextTrackPiece.type == Straight) ? 1.0 : 0;
        }
        else
        {
            *throttle = 1.0;
        }
    }
    else
    {
        /**
         * There are three angles to consider
         * slip angle - alpha - the angle between the tire’s direction of heading and the direction of travel
         * side slip angle - beta - the angle between the longitudinal axis and the actual direction of travel.
         * steering angle - delta
         *
         * delta = (180/PI) * (L/R) + K a --- (1)
         * K - understeering gradient
         * a - lateral acceleration
         * a = M * V * V / R
         * L = length of the car
         * R = radius of the bent
         *
         * there is no mention of slip angle (alpha) so we can assume that it is zero
         * alpha = beta + ( omega * b / L ) - delta
         * so if alpha is zero then
         * beta + (omega * b/L) = delta
         * omega = theta / t
         * theta = dist / R
         * delta = beta + (dist/R*t * b/L)
         * we can use t = 1 tick
         * delta = beta + (dist/R * b/L) --- (2)
         *
         * From equation (1) and (2)
         * (180/PI) * (L/R) + K*V*V/R = beta + (dist/R * b/L) // K*M is again a constant.
         * K = (R/V*V)( beta + dist/R * b/L - (180/PI)*(L/R) )
         * K = psi * ( beta + dist/R * eta - gamma)
         *
         * This K has to be less than zero for under steer. (hopefully :P)
         *
         * side slip angle beta = -arctan(vy/vx)
         * vy is the lateral velocity
         *
         * by differentiating the position with time we will get v which will be the vector sum of vx and vy
         * then vy = v * sin(beta)
         */
        beta = currentPos->angle;
        gamma = 57.2957795131 * car->dimension->length / (double)currentTrackPiece.radius;
        eta = car->dimension->guideFlagPosition / car->dimension->length;
        squaredLatVelocity = velocity * velocity;// * sin(beta) * sin(beta);
        psi = currentTrackPiece.radius / squaredLatVelocity;
        rho = dist/currentTrackPiece.radius;

        //k = (currentPos->angle - (57.3 * car->dimension->length/currentTrackPiece.radius))/(velocity * velocity/currentTrackPiece.radius);
        kappa = psi * (-beta + (rho * eta) - gamma);

        if(beta < 0)
            *throttle = 0.4;
        else
            *throttle = 0;

        *throttle = 0;
//        if(*throttle >= 1.0)
//            *throttle = 1.0;
//
//        if(*throttle < 0)
//            *throttle = 0.2;

        //printf("k: %lf slipAngle: %lf angle: %lf throttle: %lf\n", kappa, currentPos->angle, currentTrackPiece.angle, *throttle);
    }

    /* Add a pinch of randomness */
//    if (*throttle < .99)
//	    *throttle = (rand() % 10) / 10.0;
//
//    if (*throttle  > 1.00001)
//	    *throttle = 1;

//    if(lastSpeed != *throttle)
//    {
//        printf("Track type(%d) index(%d) slip(%lf) Gametick(%ld) ", currentTrackPiece.type, currentTrackPieceIndex, currentPos->angle, currentPos->tick);
//        printf("Speed (%lf) -> (%lf)\n", lastSpeed, *throttle);
//    }
}


int GetTurboUsableTrackIndex(Track* track)
{
    int pieceIndex = 0;
    for(pieceIndex=track->count-1; pieceIndex >= 0; pieceIndex--)
    {
        if(track->pieces[pieceIndex].type == Bend)
            break;
    }
    return pieceIndex;
}
